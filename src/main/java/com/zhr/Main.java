package com.zhr;

import com.zhr.controller.ImageViewerController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.net.URL;

/**
 * 这是本软件的主入口,要运行本软件请直接运行本类就可以了,不用传入任何参数
 *
 * @author ZHR
 */
public class Main extends Application {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    @Override
    public void start(Stage primaryStage) throws Exception {
        URL url = Thread.currentThread().getContextClassLoader().getResource("fxml/ImageViewer.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        Parent root = fxmlLoader.load();
        primaryStage.setTitle("图片浏览器");
        primaryStage.setResizable(true);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
        primaryStage.getIcons().add(new Image("/icons/icon.png"));
        ImageViewerController controller = fxmlLoader.getController();
        controller.setStage(primaryStage);
        controller.init();
    }


    public static void main(String[] args) {
        String version = System.getProperty("java.version");
        boolean versionFlag = Integer.parseInt(version.substring(0, 1)) == 1 && Integer.parseInt(version.substring(2, 3)) >= 8
                && Integer.parseInt(version.substring(6)) >= 60 || Integer.parseInt(version.substring(0, 1)) >= 9;
        if (versionFlag) {
            launch(args);
        } else {
            JFrame jFrame = new JFrame("版本错误");
            jFrame.setSize(500, 100);
            jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            JPanel jPanel = new JPanel();
            JLabel jLabel = new JLabel("JDK的版本不能低于1.8.0.60，请升级至最近的JDK 1.8再运行此软件，当前版本:" + version);
            jPanel.add(jLabel);
            jFrame.add(jPanel);
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
        }
    }
}
