## javaFx 图片浏览器

![示例](https://images.gitee.com/uploads/images/2020/0131/162959_e95b3a5f_1899404.png "describe.png")

### 要求
JDK <strong>1.8</strong>以上版本
### 启动本软件
* 方法一: 自助构建
```bash
    mvn jfx:jar
    cd target/jfx/app/
    java -jar ${app.name}.jar
```
* 方法二: IDE中运行
Eclipse or IntelliJ IDEA中启动, 找到```Main```类并运行就可以了
* 方法三：打包为本地原生应用，双击快捷方式即可启动，方便快捷
  如果不想打包后的安装包logo为Java的灰色的茶杯，需要在pom文件里将对应操作系统平台的图标注释放开
```bash
	#<icon>${project.basedir}/package/windows/icon.ico</icon>为windows
	#<icon>${project.basedir}/package/macosx/icon.icns</icon>为mac
	mvn jfx:native
```
另外需要注意，windows系统打包成exe的话需要安装WiXToolset3+的环境；由于打包后会把jre打入安装包，两个平台均100M左右，体积较大请自行打包；打包后的安装包在target/jfx/native目录下